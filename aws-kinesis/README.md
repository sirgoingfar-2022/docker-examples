# aws-kinesis

## What is `aws-kinesis`
`aws-kinesis` is an example on how you could setup a local instance of AWS KINESIS. 
This is based on `kinesalite` project which is a NodeJS app. 

## Usage
3 containers are created in this docker-compose.yml

* `kinesis-mock` - This is listening to port 4567. No SSL.
* `kinesis-mock-proxy` - This is the proxy server for SSL termination. Host is listing at port 8443.
* `dynamodb-mock` - This is listening at port 8000. Host is listening at port 8100. No SSL support.

## References
* https://github.com/mhart/kinesalite
* https://blog.ruanbekker.com/blog/2019/06/22/play-with-kinesis-data-streams-for-free
* https://sachabarbs.wordpress.com/2018/09/17/aws-kinesis
* https://medium.com/@ryanbrookepayne/aws-kinesis-stream-adding-data-records-1a0159412c3d
