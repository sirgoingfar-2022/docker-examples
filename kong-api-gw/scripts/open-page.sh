#!/usr/bin/env bash

unameOut="$(uname -s)"
if [[ ${unameOut} = 'Linux' ]]; then
    xdg-open $1
elif [[ ${unameOut} = 'Darwin' ]]; then
    open $1
else
    echo $1
fi